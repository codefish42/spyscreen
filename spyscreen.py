#! /usr/bin/env python3
"""
A script that takes screenshots of the screen
by <chilowi@u-pem.fr>

Prerequisites:

- Python 3

* For Linux:
- ImageMagick (it is enough if you use a X server)
- or scrot (for X server)
- or Flameshot + ImageMagick (Wayland)

* For Windows or MacOS:
- Python Imaging Library

To install Python Imaging Library (required for Windows or MacOS):
> python3 -m pip install --upgrade pip
> python3 -m pip install --upgrade Pillow
Note: sometimes the Python executable is named 'python' or 'py'

How to use?
1- Start the script in a terminal: "python3 spyscreen.py"
2- While the terminal is open, the script takes screenshots periodically.
   Screenshots are stored in the ~/spyscreen directory.
   Check if there is an error when you start the script;
   in this case screenshoting will not work.
3- When the terminal is closed (the script receives a termination signal),
   a zip archive with all the screenshots is created in ~/spyscreen.
   
Note for MacOS users: you must authorize the terminal to take screenshots
in the preferences of the system, like explained here:
https://support.apple.com/guide/mac-help/control-access-to-screen-recording-on-mac-mchld6aa7d23/mac
otherwise the screenshots will be blank.
"""

import os, sys, subprocess, datetime, shutil, random, time, hashlib, signal

MIN_PERIOD = 10.0
MAX_PERIOD = 130.0
DESTINATION = os.path.expanduser(os.path.join('~','spyscreen'))
QUALITY = 40
IMAGE_FORMATS = ['webp', 'jpg', 'png']

def hash_file(filepath):
	d = hashlib.sha256()
	with open(filepath, 'rb') as f:
		buff = f.read(4096)
		while buff:
			d.update(buff)
			buff = f.read(4096)
	return d.digest()
	
def pil_screenshot(destination, picture_format='jpg'):
	# works for Windows only
	from PIL import ImageGrab
	snapshot = ImageGrab.grab()
	snapshot.save(destination + '.{}'.format(picture_format), quality=QUALITY)

def im_screenshot(destination, picture_format='jpg'):
	subprocess.run(['import', '-window', 'root', '-quality', str(QUALITY), destination + '.{}'.format(picture_format)], stderr=subprocess.DEVNULL, check=True)

def scrot_screenshot(destination):
	subprocess.run(['scrot', '-q', str(QUALITY), destination + '.jpg'], check=True)
	
def flameshot_screenshot(destination):
    # using flameshot for wayland screenshoting
    with subprocess.Popen(['flameshot', 'full', '-d', '1', '--raw'], stdout=subprocess.PIPE) as p:
        subprocess.run(['convert', 'png:-', '-quality', str(QUALITY), destination + '.jpg'], stdin=p.stdout, check=True)
    
	
def screencapture_screenshot(destination, picture_format='jpg'):
	# last resort for MacOS (png files are heavy)
	subprocess.run(['screencapture', '-C', '-t', picture_format, '-x', destination + '.{}'.format(picture_format)], check=True)
	
def make_screenshot(destination):
	errors = []
	for f in (pil_screenshot, im_screenshot, scrot_screenshot, screencapture_screenshot, flameshot_screenshot):
		try:
			f(destination)
			print("Screenshot done using {} saved at path {}".format(f.__name__, destination), file=sys.stderr)
			return True
		except:
			import traceback
			errors.append(traceback.format_exc())
			pass
	with open(destination + '.err', 'w') as f:
		for error in errors:
			f.write(error + '\n')
	print("Error, screenshoting does not work! Have you installed the required libraries?", file=sys.stderr)
	return False
	
class Terminator(object):
	def __init__(self):
		self.terminated = False
		for sig in ('SIGINT', 'SIGTERM', 'SIGHUP', 'SIGBREAK', 'CTRL_C_EVENT', 'CTRL_BREAK_EVENT'):
			try:
				signal.signal(getattr(signal, sig), self.terminate)
			except:
				pass

	def terminate(self, signum, frame):
		print('Terminated.', file=sys.stderr)
		self.terminated = True
	
def make_screenshots(min_period, max_period, destination, interrupter):
	t = datetime.datetime.now()
	directory_name = "{}-{}-{}".format(t.year, str(t.month).zfill(2), str(t.day).zfill(2))
	os.makedirs(os.path.join(destination, directory_name), exist_ok=True)
	try:
		serial = 0
		digest = bytes(256)
		while not interrupter():
			t = datetime.datetime.now()
			filename = "{}{}{}-{}-{}".format(str(t.hour).zfill(2), str(t.minute).zfill(2), str(t.second).zfill(2), str(serial).zfill(4), digest[0:16].hex())
			filepath = os.path.join(destination, directory_name, filename)
			result = make_screenshot(filepath)
			complete_filepath = None
			if result:
				for ext in IMAGE_FORMATS:
					candidate_filepath = filepath + '.' + ext
					if os.path.exists(candidate_filepath):
						complete_filepath = candidate_filepath
						break
			if not complete_filepath:
				print('Screenshoting is not working', file=sys.stderr)
			else:
				fp = hash_file(complete_filepath)
				digest = bytes(a ^ b for (a, b) in zip(digest, fp))
			serial += 1
			wait_deadline = time.monotonic() + random.uniform(min_period, max_period)
			while not interrupter() and time.monotonic() < wait_deadline:
				time.sleep(0.5)
	finally:
		# make a zip file for the screenshots of the day
		shutil.make_archive(os.path.join(destination, 'screenshots-{}'.format(directory_name)), 'zip', os.path.join(destination, directory_name))
		print("Your screenshot archive is available at path {}".format(os.path.join(destination, directory_name + '.zip')))
		
def main():
	t = Terminator()
	make_screenshots(MIN_PERIOD, MAX_PERIOD, DESTINATION, interrupter=lambda: t.terminated)
	
if __name__ == "__main__":
	main()
