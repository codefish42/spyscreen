A Python script that takes screenshots of the screen in a directory
by <chilowi@u-pem.fr>

Please read the header of the Python script for further information.
It may be useful to watch students during computerized exams.
It should work under Linux, Windows or MacOS.
This script is released in public domain.
